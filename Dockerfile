FROM node:16.20.2-alpine3.18 as builder
COPY ./ ./
RUN npm install && npm run build
EXPOSE 8080
CMD ["npm", "start"]
